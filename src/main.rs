pub mod app;
pub mod crypt;
pub mod db;
pub mod errors;
pub mod event;
pub mod handlers;
pub mod tui;
pub mod ui;

#[macro_use]
extern crate log;
extern crate core;

use std::io;
use std::path::Path;

use log::error;
use ratatui::backend::CrosstermBackend;
use ratatui::Terminal;
use rpassword;

// -----------------------------------------------------------------------------

// fn add( password: String ) {
//     println!("Please enter name of the item you'd like to add: ");
//     let mut name = String::new();
//     io::stdin().read_line(&mut name).expect("Failed to read line");

//     println!("Please enter username of the item you'd like to add: ");
//     let mut username = String::new();
//     io::stdin().read_line(&mut username).expect("Failed to read line");

//     let item_pass = rpassword::prompt_password("Please enter password of the item you'd like to add: ").unwrap();
//     let password = crypt::encrypt(password, item_pass);

//     let pass = hex::encode(password);
//     db::Password::add(name.clone().trim().to_string(), username.trim().to_string(), pass).expect("Failed to add password");

//     println!("Password {} added", name);
// }

fn get( password: String ) {
    println!("Please enter name of the item you'd like to get: ");
    let mut name = String::new();
    io::stdin().read_line(&mut name).expect("Failed to read line");

    let value = db::Password::get_by_name(name.clone().trim().to_string());

    if value.is_err() {
        error!("Could not find password for item: {:?}", name);
        return;
    }

    let value = value.unwrap();

    let decrypted = crypt::decrypt(password, &value.data);

    println!("Name: {}", value.name);
    println!("Username: {}", value.username);
    println!("Password: {}", String::from_utf8( decrypted ).unwrap());
}

// fn list(user_pass: String) {
//     let passwords = db::Password::list().expect("Failed to list passwords");

//     for password in passwords {
//         println!("Name: {}", password.name);
//         println!("Username: {}", password.username);
//         let decrypted = crypt::decrypt(user_pass.clone(), &password.data);
//         println!("Password: {}", String::from_utf8( decrypted ).unwrap());
//     }
// }

fn create_user() -> String {
    println!("Please enter name of the user you'd like to add: ");
    let mut name = String::new();
    io::stdin().read_line(&mut name).expect("Failed to read line");

    let user_pass = rpassword::prompt_password("Please create a master password: ").unwrap();
    let pass_hash = crypt::hash(user_pass.clone());
    db::User::add(name, pass_hash).expect("Failed to add user");
    return user_pass.clone();
}

fn init() -> bool {
    let has_user = db::User::exists().expect("No user found.");

    if !has_user {
        info!("User does not exist");

        let user_pass = create_user();
        close_database_file(user_pass);
        return true
    }

    info!("User already exists");

    false
}

// Open and close database
fn open_database_file(password: String) {
    if Path::new("./passwords.enc").exists(){
        crypt::decrypt_file(
            "./passwords.enc",
            "./passwords.db",
            password
        ).expect("Failed to decrypt database");
    } else {
        println!("[DB] Database file not found. Creating a new one...");
        let _ = db::init();
        let _ = create_user();
    }
}

fn close_database_file(password: String) {
    if Path::new("./passwords.db").exists(){
        crypt::encrypt_file(
            "./passwords.db",
            "./passwords.enc",
            password
        ).expect("Failed to encrypt database");
    } else {
        info!("[DB] Database file not found.");
    }
}

fn main() -> app::AppResult<()> {
    let password = rpassword::prompt_password("Enter your master password: ").unwrap();
    open_database_file(password.clone());

    let user = db::User::get().expect("Failed to check user");
    let is_allowed = crypt::compare_hashes(password.clone(), user.hash);

    if !is_allowed {
        error!("Invalid password");
        close_database_file(password);
        panic!("Invalid password");
    }

    // Create an application.
    let mut app = app::App::new();
    app.set_current_user(user.id);

    // Initialize the terminal user interface.
    let backend = CrosstermBackend::new(io::stderr());
    let terminal = Terminal::new(backend)?;
    let events: event::EventHandler = event::EventHandler::new(250);
    let mut tui = tui::Tui::new(terminal, events);
    tui.init()?;

    // Start the main loop.
    while app.running {
        // Render the user interface.
        tui.draw(&mut app)?;
        // Handle events.
        match tui.events.next()? {
            event::Event::Tick => app.tick(),
            event::Event::Key(key_event) => handlers::handle_key_events(key_event, &mut app)?,
            event::Event::Mouse(_) => {}
            event::Event::Resize(_, _) => {}
        }
    }

    // Exit the user interface.
    tui.exit()?;
    close_database_file(password);
    return app::AppResult::Ok(());
}