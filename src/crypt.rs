use std::fs::{File, remove_file};
use std::io::{Read, Write};

use argon2::{
    password_hash::{
        PasswordHash, PasswordHasher, PasswordVerifier, SaltString
    },
    Argon2
};
use orion::hazardous::{
    aead::xchacha20poly1305::{seal, open, Nonce, SecretKey as XSecretKey},
    aead::streaming::{StreamTag, ABYTES},
    mac::poly1305::POLY1305_OUTSIZE,
    stream::xchacha20::XCHACHA_NONCESIZE,
};
use orion::aead::streaming::{
    StreamOpener,
    StreamSealer
};
use orion::aead::SecretKey;
use orion::hazardous::stream::chacha20::CHACHA_KEYSIZE;
use orion::kdf::{derive_key, Password, Salt};
use rand_core::{OsRng, RngCore};

const NONCE_PLUS_AD_SIZE: usize = XCHACHA_NONCESIZE + 32;

/// Split encrypted cipher text into IV, AD and encrypted text
fn split_encrypted( cipher_text: &[u8] ) -> (Vec<u8>, Vec<u8>, Vec<u8>) {
    return (
        cipher_text[..XCHACHA_NONCESIZE].to_vec(),
        cipher_text[XCHACHA_NONCESIZE..NONCE_PLUS_AD_SIZE].to_vec(),
        cipher_text[NONCE_PLUS_AD_SIZE..].to_vec(),
        )
}

/// Fill passed array with cryptographically random data from ring crate
fn get_random( dest: &mut [u8]) {
    RngCore::fill_bytes(&mut OsRng, dest);
}

fn nonce() -> Vec<u8> {
    let mut randoms: [u8; 24] = [0; 24];
    get_random( &mut randoms );
    return randoms.to_vec();
}

fn auth_tag() -> Vec<u8> {
    let mut randoms: [u8; 32] = [0; 32];
    get_random( &mut randoms );
    return randoms.to_vec();
}

fn create_key( password: String, nonce: Vec<u8> ) -> XSecretKey {
    let password = Password::from_slice(password.as_bytes()).unwrap();
    let salt = Salt::from_slice(nonce.as_slice()).unwrap();
    let kdf_key = derive_key(&password, &salt, 15, 1024, CHACHA_KEYSIZE as u32).unwrap();
    let key = XSecretKey::from_slice( kdf_key.unprotected_as_bytes() ).unwrap();
    return key;
}

fn create_standard_key( password: String, nonce: Vec<u8> ) -> SecretKey {
    let password = Password::from_slice(password.as_bytes()).unwrap();
    let salt = Salt::from_slice(nonce.as_slice()).unwrap();
    let kdf_key = derive_key(&password, &salt, 15, 1024, CHACHA_KEYSIZE as u32).unwrap();
    let key = SecretKey::from_slice( kdf_key.unprotected_as_bytes() ).unwrap();
    return key;
}

pub fn encrypt( password: String, data: String ) -> Vec<u8> {
    let nonce = nonce();
    let key = create_key( password, nonce.clone() );
    let nonce = Nonce::from_slice( nonce.as_slice() ).unwrap();
    let ad = auth_tag();

    // Get the output length
    let output_len = match data.len().checked_add( XCHACHA_NONCESIZE + POLY1305_OUTSIZE + ad.len() ) {
        Some( min_output_len ) => min_output_len,
        None => panic!( "Plaintext is too long" ),
    };

    // Allocate a buffer for the output
    let mut output = vec![0u8; output_len];
    output[..XCHACHA_NONCESIZE].copy_from_slice(nonce.as_ref());
    output[XCHACHA_NONCESIZE..NONCE_PLUS_AD_SIZE].copy_from_slice( ad.as_ref() );
    seal(&key, &nonce, data.as_bytes(), Some( ad.clone().as_slice() ), &mut output[NONCE_PLUS_AD_SIZE..]).unwrap();
    return output;
}

pub fn decrypt( password: String, cipher_text: &String ) -> Vec<u8> {
    let ciphertext = hex::decode(cipher_text).unwrap();
    let key = create_key(password, ciphertext[..XCHACHA_NONCESIZE].to_vec());
    let split = split_encrypted( ciphertext.as_slice() );
    let nonce = Nonce::from_slice( split.0.as_slice() ).unwrap();
    let mut output = vec![0u8; split.2.len()];

    open(&key, &nonce, split.2.as_slice(), Some( split.1.as_slice() ), &mut output ).unwrap();
    // Remove any remaining padding
    output.retain(|&x| x != 0u8);
    return output.to_vec();
}

pub fn hash( password: String ) -> String {
    let salt = SaltString::generate(&mut OsRng);

    // Argon2 with default params (Argon2id v19)
    let argon2 = Argon2::default();

    let password_hash = argon2.hash_password(password.as_bytes(), &salt).unwrap();

    return password_hash.to_string();
}

pub fn compare_hashes( password: String, hash: String ) -> bool {
    let parsed_hash = PasswordHash::new(&hash).expect("Failed to parse hash");
    return Argon2::default().verify_password(password.as_bytes(), &parsed_hash).is_ok();
}

// ------------------------------------------------------------------------------
// File encryption
// ----------------------------------------------------------------------------

pub fn encrypt_file(
    source_file_path: &str,
    dist_file_path: &str,
    password: String
) -> Result<(), orion::errors::UnknownCryptoError> {
    info!( "[ENC] Starting encryption..." );

    let nonce = nonce();
    let key = create_key( password, nonce.clone() );
    let nonce = Nonce::from_slice( nonce.as_slice() ).unwrap();
    let ad = auth_tag();

    let mut source_file = File::open( source_file_path ).unwrap();
    info!( "[ENC] Read source file {}", source_file_path );
    let mut dist_file = File::create(dist_file_path).unwrap();
    info!( "[ENC] Read dist file {}", dist_file_path );

    let mut contents = Vec::new();
    source_file.read_to_end(&mut contents).unwrap();

    // Get the output length
    let output_len = match contents.len().checked_add( XCHACHA_NONCESIZE + POLY1305_OUTSIZE + ad.len() ) {
        Some( min_output_len ) => min_output_len,
        None => panic!( "Plaintext is too long" ),
    };

    // Allocate a buffer for the output
    let mut output = vec![0u8; output_len];
    output[..XCHACHA_NONCESIZE].copy_from_slice(nonce.as_ref());
    output[XCHACHA_NONCESIZE..NONCE_PLUS_AD_SIZE].copy_from_slice( ad.as_ref() );
    seal(&key, &nonce, contents.as_slice(), Some( ad.clone().as_slice() ), &mut output[NONCE_PLUS_AD_SIZE..]).unwrap();
    dist_file.write( &output.as_slice()).unwrap();
    remove_file( source_file_path ).unwrap();
    info!( "[ENC] Written to dist file {}", dist_file_path );
    Ok(())
}

pub fn decrypt_file(
    encrypted_file_path: &str,
    dist: &str,
    password: String,
) -> Result<(), orion::errors::UnknownCryptoError> {
    info!( "[DEC] Starting decryption..." );

    let mut encrypted_file = File::open(encrypted_file_path).unwrap();
    let mut dist_file = File::create(dist).unwrap();

    let mut contents = Vec::new();
    encrypted_file.read_to_end(&mut contents).unwrap();
    info!( "[DEC] Read source file {}", encrypted_file_path );
    info!( "[DEC] Content length {}", contents.len() );

    let key = create_key(password, contents[..XCHACHA_NONCESIZE].to_vec());
    let split = split_encrypted( contents.as_slice() );
    let nonce = Nonce::from_slice( split.0.as_slice() ).unwrap();
    let mut output = vec![0u8; split.2.len()];

    open(&key, &nonce, split.2.as_slice(), Some( split.1.as_slice() ), &mut output ).unwrap();
    dist_file.write( &output.as_slice()).unwrap();
    remove_file( encrypted_file_path ).unwrap();
    info!( "[DEC] Written to dist file {}", dist);
    Ok(())
}

// const CHUNK_SIZE: usize = 128; // The size of the chunks you wish to split the stream into.

// pub fn encrypt_large_file(
//     file_path: &str,
//     output_path: &str,
//     password: String
// ) -> Result<(), orion::errors::UnknownCryptoError> {
//     let mut input_file = File::open(file_path).expect("Failed to open input file");
//     let mut output_file = File::create(output_path).expect("Failed to create output file");

//     let mut src = Vec::new();
//     input_file.read_to_end(&mut src).expect("Failed to read input file");
//     let mut out: Vec<Vec<u8>> = Vec::with_capacity(src.len() / CHUNK_SIZE);

//     let nonce = nonce();
//     let key = create_standard_key(password, nonce);

//     // Encryption:
//     let (mut sealer, nonce) = StreamSealer::new(&key)?;

//     println!("nonce.len: {}", nonce.len());

//     out.push(nonce.as_ref().to_vec());

//     for (n_chunk, src_chunk) in src.chunks(CHUNK_SIZE).enumerate() {
//         let encrypted_chunk =
//             if src_chunk.len() != CHUNK_SIZE || n_chunk + 1 == src.len() / CHUNK_SIZE {
//                 // We've reached the end of the input source,
//                 // so we mark it with the Finish tag.
//                 sealer.seal_chunk(src_chunk, &StreamTag::Finish)?
//             } else {
//                 // Just a normal chunk
//                 sealer.seal_chunk(src_chunk, &StreamTag::Message)?
//             };
//         // Save the encrypted chunk somewhere
//         out.push(encrypted_chunk);
//     }

//     output_file.write_all(&out.concat()).expect("Failed to write to output file");

//     Ok(())
// }


// pub fn decrypt_large_file(
//     file_path: &str, 
//     output_path: &str,
//     password: String
// ) -> Result<(), orion::errors::UnknownCryptoError> {
//     let mut input_file = File::open(file_path).expect("Failed to open input file");
//     let mut output_file = File::create(output_path).expect("Failed to create output file");

//     let mut src: Vec<u8> = Vec::new();
//     input_file.read_to_end(&mut src).expect("Failed to read input file");
//     let mut out: Vec<Vec<u8>> = Vec::with_capacity(src.len() / CHUNK_SIZE);

//     print!("src.len = {}\n", src.len());

//     let nonce = src[..XCHACHA_NONCESIZE].to_vec();

//     print!("nonce.len = {}\n", nonce.len());

//     src = src[XCHACHA_NONCESIZE..].to_vec();
//     let key = create_standard_key(password, nonce.clone());
//     let nonce = Nonce::from_slice( nonce.as_slice() ).unwrap();

//     print!("corrected src.len = {}\n", src.len());

//     print!("Got key and nonce\n");

//     let mut opener = StreamOpener::new(&key, &nonce)?;

//     print!("Got opener\n");

//     let mut processed = 0;

//     for (n_chunk, src_chunk) in src.chunks(CHUNK_SIZE).enumerate() {
//         let (_decrypted_chunk, tag) = opener.open_chunk(src_chunk)?;

//         print!("{} {} {}", _decrypted_chunk.len(), _decrypted_chunk.len() / CHUNK_SIZE, CHUNK_SIZE );

//         if src_chunk.len() != CHUNK_SIZE + ABYTES || n_chunk + 1 == out.len() {
//             // We've reached the end of the input source,
//             // so we check if the last chunk is also set as Finish.
//             assert_eq!(tag, StreamTag::Finish, "Stream has been truncated!");
//         }
//         out.push(_decrypted_chunk);

//         println!("Processed {} chunks", processed);
//         processed += 1;
//     }

//     output_file.write_all(&out.concat()).expect("Failed to write to output file");

//     Ok(())
// }