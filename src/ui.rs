use ratatui::{
    layout::{Alignment, Layout, Constraint, Direction},
    style::{Color, Style},
    widgets::{Block, BorderType, Paragraph, Borders, Dataset, List, ListItem},
    widgets::block::*,
    Frame,
};

use crate::app::App;
use crate::db::{User, Password};

fn users_widget() -> List<'static> {
    let binding = User::list()
        .unwrap();

    let widgets = binding
        .iter()
        .map(|user| ListItem::new(user.name.to_string()))
        .collect::<Vec<ListItem>>();

    List::new(widgets)
        .block(
            Block::default()
                .title("Users".as_ref())
                .borders(Borders::ALL),
        )
        .highlight_style(Style::default().fg(Color::LightGreen))
}

fn passwords_widget(user_id: i32) -> List<'static> {
    let passwords = Password::list(user_id)
        .unwrap();

    if passwords.len() == 0 {
        return List::new(vec![ListItem::new("No passwords found".to_string())])
            .block(
                Block::default()
                    .title("Passwords".as_ref())
                    .borders(Borders::ALL),
            )
            .highlight_style(Style::default().fg(Color::LightGreen))
    }

    let password_widgets = passwords
        .iter()
        .map(|password| ListItem::new(password.name.to_string()));
    
    List::new(password_widgets)
        .block(
            Block::default()
                .title("Passwords".as_ref())
                .borders(Borders::ALL),
        )
        .highlight_style(Style::default().fg(Color::LightGreen))
}

fn shortcuts() -> List<'static> {
    let shortcuts = vec![
        ("ESC -> Quit"),
        ("q -> Quit"),
        ("a -> Add password"),
    ];

    List::new(shortcuts)
        .block(
            Block::default()
                .title("Shortcuts".as_ref())
                .borders(Borders::ALL),
        )
        .highlight_style(Style::default().fg(Color::LightGreen))
}

/// Renders the user interface widgets.
pub fn render(app: &mut App, frame: &mut Frame) {
    // This is where you add new widgets.
    // See the following resources:
    // - https://docs.rs/ratatui/latest/ratatui/widgets/index.html
    // - https://github.com/ratatui-org/ratatui/tree/master/examples
    let layout = Layout::default()
        .direction(Direction::Vertical)
        .margin(1)
        .constraints(vec![
            Constraint::Percentage(90),
            Constraint::Percentage(10),
        ])
        .split(frame.size());

    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(30), Constraint::Percentage(70)].as_ref())
        .split(layout[0]);

    let footer_chunks = Layout::new(
        Direction::Horizontal, 
        [Constraint::Percentage(100)].as_ref()
    ).split(layout[1]);
    
    let header = Block::default()
        .borders(Borders::ALL)
        .title(Title::from("password_manager").alignment(Alignment::Center))
        .title_style(Style::default().fg(Color::Cyan));
    
    frame.render_widget(header, frame.size());


    let list = users_widget();
    let password_list = passwords_widget(app.current_user_id.unwrap());

    frame.render_stateful_widget(
        list, 
        chunks[0],
        &mut Default::default()
    );

    frame.render_stateful_widget(
        password_list,
        chunks[1],
        &mut Default::default()
    );

    frame.render_widget(shortcuts(), footer_chunks[0]);
}