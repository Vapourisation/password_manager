use std::error;
use std::fmt;

#[derive(Debug, Clone)]
pub struct InvalidPassword;

impl fmt::Display for InvalidPassword {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid password")
    }
}

impl error::Error for InvalidPassword {
    fn description(&self) -> &str {
        "Invalid password"
    }
}