use std::error;
use std::io;

use rpassword;

use crate::db::{User, Password};

/// Application result type.
pub type AppResult<T> = std::result::Result<T, Box<dyn error::Error>>;

/// Application.
#[derive(Debug)]
pub struct App {
    /// Is the application running?
    pub running: bool,
    /// counter
    pub counter: u8,

    pub current_user_id: Option<i32>,
}

impl Default for App {
    fn default() -> Self {
        Self {
            running: true,
            counter: 0,
            current_user_id: None,
        }
    }
}

impl App {
    /// Constructs a new instance of [`App`].
    pub fn new() -> Self {
        Self::default()
    }

    pub fn set_current_user(&mut self, user_id: i32) {
        self.current_user_id = Some(user_id);
    }

    pub fn add_password(&self) {
        println!("Enter a name for the password you'd like to add: ");
        let mut name = String::new();
        io::stdin().read_line(&mut name).expect("Failed to read line");

        println!("Enter the username/email for the password you'd like to add: ");
        let mut user = String::new();
        io::stdin().read_line(&mut name).expect("Failed to read line");

        let pass = rpassword::prompt_password("Enter the password: ").unwrap();
        Password::add(self.current_user_id.unwrap(), name, user, pass).unwrap();
    }

    

    /// Handles the tick event of the terminal.
    pub fn tick(&self) {}

    /// Set running to false to quit the application.
    pub fn quit(&mut self) {
        self.running = false;
    }

    pub fn increment_counter(&mut self) {
        if let Some(res) = self.counter.checked_add(1) {
            self.counter = res;
        }
    }

    pub fn decrement_counter(&mut self) {
        if let Some(res) = self.counter.checked_sub(1) {
            self.counter = res;
        }
    }
}