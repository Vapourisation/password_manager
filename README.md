# password_manager

## Yet Another Silly Little Side-Project

Yep, that's right, another silly little side-project. Someone, somewhere, was talking about some projects they were working on and mentioned a password manager and SOMEHOW I've never written a password manager despite creating general encryption functions and even a password/passphrase generator function. So, I figured I might as well make a little toy CLI password manager to learn a little more Rust. It turns out I didn't really learn much Rust with this, just used some libraries and code I've already used. Bit of a bummer.

I just wanted to toy around with a password manager, figure out how they generally work and get a better understanding of the technology.

## Anyway, wanna run this?

> As of 3rd March 2024

It's pretty easy. Just build it:

```bash
cargo build
```

When that's done it'll generate an executable for whatever OS you're on. So you can run it with these commands:

### init
```bash
./target/debug/password_manager init
```
Checks if a master user exists, if not, asks you to create one

### get
```bash
./target/debug/password_manager get
```
Checks a user exists, if not, prompt you to create one with a `master_password`. If a user does exist, it'll ask you for your `master_password`, then check that they match. If they match, it'll ask for the name of the item you want to get, and if it exists, returns it.

### add
```bash
./target/debug/password_manager add
```
Checks a user exists, if not, prompt you to create one with a `master_password`. If a user does exist, it'll ask you for your `master_password`, then check that they match. If they match, it'll ask for the name, username and password of the item you want to add, and then adds it.


## TODO

- [x] Properly password lock/encrypt the entire database 
    - [] I think this could definitely be done better. It's currently done all in one go and for the entire file which is going to just take longer and require more memory the larger the file is. `orion` supports streaming encryption so I _should_ be able to chunk the file.
    - Need to check how other tools handle this
- [] Session based actvity
    - You should only need to enter your master password when opening the tool. After that it should allow you to query items for a set amount of time before locking the database again
- [] Allow get/add commands to accept args so that you can pass the values you want to get/add without needing further inputs